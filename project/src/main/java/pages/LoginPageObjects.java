package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPageObjects {
	
	WebDriver driver = null;
	
	By emailField = By.xpath("//*[@id=\\\"email\\\"]");
	By loginButton = By.xpath("//*[@id=\\\"SubmitLogin\\\"]");
	By passField = By.xpath("//*[@id=\\\"passwd\\\"]");
	By emailCreateAccountTextField = By.xpath("//*[@id=\\\"email_create\\\"]");
	By emailCreateAccountButton = By.xpath("//*[@id=\\\"SubmitCreate\\\"]");
	
	public LoginPageObjects(WebDriver driver) {
		this.driver = driver;
	}
	
	
	public void setLoginEmail(String text) {
		driver.findElement(emailField).sendKeys(text);
	}
	
	public void setLoginPassword(String text) {
		driver.findElement(passField).sendKeys(text);
	}
	
	public void clickLoginButton() {
		driver.findElement(loginButton).click();
	}
	
	public void setAccountEmail(String text) {
		driver.findElement(emailCreateAccountTextField).sendKeys(text);
	}
	
	public void clickAccountCreationButton(){
		driver.findElement(emailCreateAccountButton).click();
	}
}
